<?php
require ('fonctions.php');
$bdd = getDatabase();
// répertoire qui contient toutes les images
$pictures_directory = 'Pictures/';


// map qui associe aux extensions le mimetype correspondant
$map_extension_mimetype = [
    'jpg' => 'image/jpeg',
    'jpeg' => 'image/jpeg',
    'png' => 'image/png',
    'gif' => 'image/gif',
];

if (empty($_GET['image'])) {
    return;
}
// on récupére l'id de l'image
$image = (int) $_GET['image'];


$filename = getPictureName($bdd);

// chemin complet de l'image
$path = $pictures_directory . '/' . $filename;
// extension de l'image
$extension = pathinfo($filename, PATHINFO_EXTENSION);

// entête HTTP pour le type de fichier
header('Content-Type: ' . $map_extension_mimetype[$extension]);
// entête HTTP pour la taille du fichier
header('Content-Length: ' . filesize($path));

// envoi du fichier au navigateur
readfile($path);

?>