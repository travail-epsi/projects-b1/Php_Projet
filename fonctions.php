<?php
/*
 * Connexion à la base de données hebergée sur Ingenium
*/
function getDataBase()
{
    try {
        $bdd = null;
        $bdd = new PDO('mysql:host=mysql;
                            port=3306;
                            dbname=neptune;
                            charset=utf8',
            'root',
            'password',
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    } catch (Exception $exception) {
        die($exception);
    }
    return $bdd;
}

/*
 * Récupération de tout ce qui se trouve dans la table client dans la base de données utilisé pour la recherche de clients sur la page ListeClients.php
*/
function getAllClients(PDO $bdd, $nom)
{
    $query = "SELECT * 
              FROM clients 
              WHERE nom LIKE :p_nom";
    $clients = null;
    $statement = $bdd->prepare($query);
    $nom = $nom . '%';
    $statement->bindParam(':p_nom', $nom);

    if ($statement->execute()) {
        $clients = $statement->fetchAll(PDO::FETCH_OBJ);
        // Fermeture de la ressource
        $statement->closeCursor();
    }
    return $clients;
}

/*
 * Récupération de tout ce qui se trouve dans la table client dans la base de données utilisé pour la mise à jour des clients
*/
function getModifClient(PDO $bdd, $id)
{
    // Etape 2 : Obtention de l'éditeur depuis la BD
    $query = "SELECT * FROM clients WHERE id=:p_id";
    // Etape 2.1
    $statement = $bdd->prepare($query);
    // Etape 2.2
    $statement->bindParam(':p_id', $id);
    // Etape 2.3
    if ($statement->execute()) {
        // On récupère le publisher au format objet
        $client = $statement->fetch(PDO::FETCH_OBJ);
    }
    return $client;
}

/*
 * Récupération des pays stockés dans la base de données pour créer un burger menu dans la modification/ajout de clients
*/
function getCountries(PDO $bdd)
{
    $query = "SELECT * FROM pays";
    $stmt = $bdd->prepare($query);
    if ($stmt->execute()) {
        $pays = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    return $pays;

}

/*
 * Récupération de tout ce qui se trouve dans la table admins de la bdd pour la connexion
*/
function getAdmin(PDO $bdd, $email)
{
    $query = "SELECT * 
              FROM admins 
              WHERE email = :p_email";
    $admin = null;
    $statement = $bdd->prepare($query);
    $statement->bindParam(':p_email', $email);

    if ($statement->execute()) {
        $admin = $statement->fetch(PDO::FETCH_OBJ);
        // Fermeture de la ressource
        $statement->closeCursor();
    }
    return $admin;
}


function getAllRooms(PDO $bdd, $localisation)
{
    $query = "SELECT *
               FROM chambres as c, tarifs as t
               WHERE c.exposition LIKE :c_expo and t.id = c.tarif_id";

    $rooms = null;
    $statement = $bdd->prepare($query);
    $localisation = $localisation . '%';

    $statement->bindParam(':c_expo', $localisation);

    if ($statement->execute()) {
        $rooms = $statement->fetchAll(PDO::FETCH_OBJ);
        $statement->closeCursor();
    }
    return $rooms;
}


function getRooms(PDO $bdd, $room)
{
    $query = "SELECT *
               FROM chambres as c
               WHERE c.numero LIKE :c_numero";

    $rooms = null;
    $statement = $bdd->prepare($query);
    $room = $room;

    $statement->bindParam(':c_numero', $room);

    if ($statement->execute()) {
        $rooms = $statement->fetchAll(PDO::FETCH_OBJ);
        $statement->closeCursor();
    }
    return $rooms;
}


function getAllRoomsMoney(PDO $bdd, $localisation, $prix)
{
    $query = "SELECT *
               FROM chambres as c, tarifs as t
               WHERE c.exposition LIKE :c_expo AND t.id = c.tarif_id";

    if (!empty($prix)) {
        $query = $query . " and t.prix <= :t_price";
    }
    $rooms = null;
    $statement = $bdd->prepare($query);
    $localisation = $localisation . '%';

    $statement->bindParam(':c_expo', $localisation);
    if (!empty($prix)) {
        $statement->bindParam(":t_price", $prix);
    }
    if ($statement->execute()) {
        $rooms = $statement->fetchAll(PDO::FETCH_OBJ);
        $statement->closeCursor();
    }
    return $rooms;
}


function getMoney(PDO $bdd)
{
    $query = "SELECT * 
              FROM tarifs as t";
    $statement = $bdd->prepare($query);
    if ($statement->execute()) {
        $tarifs = $statement->fetchAll(PDO::FETCH_OBJ);
    }
    return $tarifs;
}

function getPictureName($bdd)
{
    $query = "SELECT * FROM images as i, chambres as c WHERE i.numero_chambre =  c.numero";
    $statement = $bdd->prepare($query);
    if ($statement->execute()) {
        $imageName = $statement->fetchAll(PDO::FETCH_OBJ);
    }
    return $imageName;
}

