<?php
require('fonctions.php');

$query= "DELETE FROM clients WHERE id=:p_id";

$bdd=getDataBase();
$stmt = $bdd->prepare($query);
$stmt->bindParam(':p_id',$_POST['id'] );
try {
    if($stmt->execute()){
        header('Location: ListeClients.php');
    }
}catch (Exception $exception){
    echo '<script>alert("Impossible de supprimer le client")</script>';
    echo '<script>window.location.replace("ListeClients.php");</script>';
}
