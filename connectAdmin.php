<?php
if (!isset($_POST['mdp']) || !isset($_POST['email'])) {
    echo '<script>alert("Mot de passe ou Email invalide")</script>';
    echo '<script>window.location.replace("Connexion.php");</script>';
    die();
}
require('fonctions.php');
$bdd = getDataBase();
$email = htmlspecialchars($_POST['email']);
$mdp = htmlspecialchars($_POST['mdp']);
$admin = getAdmin($bdd, $email);

if (!empty($admin)) {
    if (password_verify($mdp, $admin->mdp)) {
        echo '<script>window.location.replace("Administration.php");</script>';
        die();
    } else {
        echo '<script>alert("Mot de Passe invalide")</script>';
        echo '<script>window.location.replace("Connexion.php");</script>';
    }
} else {
    echo '<script>alert("Email invalide")</script>';
    echo '<script>window.location.replace("Connexion.php");</script>';
}