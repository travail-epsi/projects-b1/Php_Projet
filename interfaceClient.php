<html>
<body>

<h1>Liste des chambres</h1>

<?php
$localisation = '';
if(isset($_POST['localisation'])){
    $localisation=htmlspecialchars($_POST['localisation']);
}

$prix = '';
if(isset($_POST['localisation'])){
    $prix=htmlspecialchars($_POST['prix']);
}
?>

<form action="" method="post">
    <label for="localisation">La chambre commence par :</label>
    <input type="text" name="localisation" value="<?= $localisation ?>"/>
    <label for="prix">La chambre coute moins de :</label>
    <input type="text" name="prix" value="<?= $prix ?>"/>
    <input type="submit" value="Rechercher"/>
</form>
<br>

<a href="ajouterChambre.php">Ajouter</a><br>

<?php
require('fonctions.php');

$bdd= getDatabase();
$chambres= null;
if($bdd){
    $chambres = getAllRoomsMoney($bdd, $localisation, $prix);

    if($chambres){
        foreach ($chambres as $chambre){
            echo 'N°=' . $chambre->numero . ', Capacite='
                . $chambre->capacite . ', Exposition=' . $chambre->exposition .
                ', douches=' . $chambre->douche . ', etage=' . $chambre->etage . ', tarif='
                . $chambre->prix . '€';

        }
    } else {
        echo '<script>alert("Aucune chambre correspondant à ses critères")</script>';
    }
} else {
    echo "Erreur d'accès au serveur";
}
?>
</body>
</html>