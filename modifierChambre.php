<html>
<body>
<h1>Modifier une chambre</h1>

<?php
require ('fonctions.php');

$chambre = null;
if(isset($_GET['numero'])){
    $numero = $_GET['numero'];

    $bdd = getDatabase();

    $query = "SELECT * FROM chambres WHERE numero=:n_umber";

    $statement = $bdd->prepare($query);
    $statement->bindParam(':n_umber', $numero);

    if($statement->execute()){
        $chambre = $statement->fetch(PDO::FETCH_OBJ);
    }
}

if ($chambre == null){
    echo 'Aucune exposition à cette endroit';
} else {
    ?>

<form action="updateChambre.php" method="post">
    <label for="capacite">Capacite :</label>
    <input type="text" name="capacite" value="<?= $chambre->capacite?>"/> <br />
    <label for="exposition">Exposition :</label>
    <input type="text" name="exposition" value="<?= $chambre->exposition?>"/> <br />
    <label for="douche">douche :</label>
    <input type="text" name="douche" value="<?= $chambre->douche ?>"/> <br />
    <label for="etage">Etage :</label>
    <input type="text" name="etage" value="<?= $chambre->etage?>"/> <br />
    <label for="tarif_id">Tarif :</label>
    <input type="text" name="tarif_id" value="<?= $chambre->tarif_id ?>"/> <br />

    <input type="hidden" name="numero" value="<?= $chambre->numero ?>" />
    <input type="submit" value="Valider">
</form>
<?php
}
?>
</body>
</html>