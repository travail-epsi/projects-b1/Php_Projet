<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
</head>
<body>
<title>Inscription</title>

<form action="addAdmin.php" method="post" class="">

    <div class="form-group">
        <label for="nom">Nom : </label>
        <input type="text" name="nom" value="" placeholder="Holmes">
    </div>

    <div class="form-group">
        <label for="prenom">Prénom : </label>
        <input type="text" name="prenom" value="" placeholder="John">
    </div>

    <div class="form-group">
        <label for="email">Adresse Mail: </label>
        <input type="email" name="email" value="" placeholder="prenom.nom@neptune.fr">
    </div>

    <div class="form-group">
        <label for="mdp">Mot de Passe: </label>
        <input type="password" name="mdp" value="" placeholder="password">
    </div>

    <button type="submit" class="btn btn-primary">Valider</button>

</form>
</body>
</html>
