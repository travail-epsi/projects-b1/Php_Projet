<?php

require ('fonctions.php');


$bdd = getDatabase();

$resultat = 1;
$numero = intval($_POST['numero']);
$chambres = getRooms($bdd, $numero);
$extensions_valides = array('jpg', 'jpeg', 'gif', 'png');
$imageName = getPictureName($bdd);


foreach (($_FILES) as $key => $file) {

    if ($_FILES[$key]['error'] > 0) { //vérification des erreurs
        //erreur
        $resultat = -1;
        echo "Erreur";
    } else if ($_FILES[$key]['size'] > 409600) { //vérification de la taille de l'image inf à 400k
        $resultat = -2;
        echo "Image trop grande";
    }

    //vérification de l'extension de l'image
    $extensions_uploaded = pathinfo($_FILES[$key]['name'], PATHINFO_EXTENSION);
    if (!in_array($extensions_uploaded, $extensions_valides)) {
        $resultat = -3;
        echo "Mauvaise extension";
    }

    if ($resultat != 1) {
        echo "erreur copie";
        continue; // on passe directement à l'image suivante (= itération suivante du foreach)
    }

    //stocker sur le disque
    $i=uniqid("", true);
    $fileName = 'f' . $numero . '_' . $i . '.' . $extensions_uploaded;
    $fullPathName = 'Pictures/' . $fileName;
    $path = 'Pictures/';
    //La copie
    $copied = move_uploaded_file($_FILES[$key]['tmp_name'], $fullPathName);

 /*   $i++;
    $fileName = 'f' . $numero . '_' . $i . '.' . $extensions_uploaded;
    if($fileName != $imageName){
        $fullPathName = '/home/lothar/Pictures/' . $fileName;
        $path = '/home/lothar/Pictures/';
        //La copie
        $copied = move_uploaded_file($_FILES[$key]['tmp_name'], $fullPathName);
    } else {
        do{
            $i++;
            $fileName = 'f' . $numero . '_' . $i . '.' . $extensions_uploaded;
        } while ($fileName == $imageName);
    }
*/

    if ($copied) {
        try {
            $query = 'INSERT INTO images (image, numero_chambre, path) VALUES (:image, :chambre, :path)';
            $statement = $bdd->prepare($query);
            $statement->bindParam(':image', $fileName);
            $statement->bindParam(':chambre', $numero);
            $statement->bindParam(':path', $path);
            $statement->execute();
            if(empty($chambres->main_picture)){
                $query2 = 'UPDATE chambres SET main_picture = :path_room WHERE numero = :c_num';
                $statement2 = $bdd->prepare($query2);
                $statement2->bindParam(':path_room', $fullPathName);
                $statement2->bindParam(':c_num', $numero);
                $statement2->execute();
            }

            echo "succès";
            header('Location: listeChambre.php');
        } catch (Exception $exception) {
            echo "Erreur bdd";
            var_dump($exception);
        }
    }
}// il faut peut être un else peut être ?
