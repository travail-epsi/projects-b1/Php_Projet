<?php
require ('fonctions.php');


$query = "UPDATE chambres
          SET capacite=:c_apacity,
          exposition=:e_xposition,
          douche=:d_shower,
          etage=:e_level,
          tarif_id=:t_id
          WHERE numero=:n_umber";

$bdd = getDatabase();
$statement=$bdd->prepare($query);
$statement->bindParam(':c_apacity', $_POST['capacite']);
$statement->bindParam(':e_xposition', $_POST['exposition']);
$statement->bindParam(':d_shower', $_POST['douche']);
$statement->bindParam(':e_level', $_POST['etage']);
$statement->bindParam(':t_id', $_POST['tarif_id']);
$statement->bindParam('n_umber', $_POST['numero']);
var_dump($statement);

if ($statement->execute()){
    header('Location: listeChambre.php');
} else {
    echo "Essaye encore";
}
