<html>
<head>
    <link href="CSSduPauvre.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
</head>
<tbody>

<div class="center">
<h1>Liste des chambres</h1>

<?php
$localisation = '';
if(isset($_POST['localisation'])){
    $localisation=htmlspecialchars($_POST['localisation']);
}

$prix = '';
if(isset($_POST['localisation'])){
    $prix=htmlspecialchars($_POST['prix']);
}
?>



<form action="" method="post">
    <label for="localisation">La chambre commence par :</label>
    <input type="text" name="localisation" value="<?= $localisation ?>"/>
    <label for="prix">La chambre coute moins de :</label>
    <input type="text" name="prix" value="<?= $prix ?>"/>
    <input type="submit" value="Rechercher"/>
</form>
<br>

    <a href="ajouterChambre.php"><button type="button" class="btn btn-primary">Ajouter</button></a>
</div>

<br>

<?php
require('fonctions.php');

$bdd= getDatabase();
$chambres= null;
if($bdd){
$chambres = getAllRooms($bdd, $localisation);
$liste_images_chambre = getPictureName($bdd);

?>
<table class="table table-striped">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Numero</th>
        <th scope="col">Capacité</th>
        <th scope="col">Exposition</th>
        <th scope="col">Douche</th>
        <th scope="col">Etage</th>
        <th scope="col">Tarif</th>
        <th scope="col">Image</th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <?php

    if ($chambres) {
            foreach ($chambres as $chambre) {
                        echo '<tr>'
                            . '<td scope="row">' . $chambre->numero . '</td>'
                            . '<td scope="row">' . $chambre->capacite . '</td>'
                            . '<td scope="row">' . $chambre->exposition . '</td>'
                            . '<td scope="row">' . $chambre->douche . '</th>'
                            . '<td scope="row">' . $chambre->etage . '</td>'
                            . '<td scope="row">' . $chambre->prix . '€' . '</td>'
                            . '<td><img src="'. $chambre->main_picture . '"></td>'


                            . '<td><a href="modifierChambre.php?numero=' . $chambre->numero . '"><button type="button" class="btn btn-primary">Modifier</button> </a></td>'
                            . '<td><a href="supprimerChambre.php?numero=' . $chambre->numero . '"><button type="button" class="btn btn-primary">Supprimer</button> </a>'
                            . '<td><a href="ajouterPhoto.php?numero=' . $chambre->numero . '"><button type="button" class="btn btn-primary">Ajouter photo</button> </a>' . '<br>';
                    }

        ?>
        </table>
        </div>

        <?php


    } else {
        echo "Aucun résultat";
    }
} else {
    echo "Erreur d'accès au serveur";
}
?>
</tbody>

</html>