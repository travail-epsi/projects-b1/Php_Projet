<html>
<body>

<h1>Supprimer une chambre</h1>

<?php
require ('fonctions.php');

$chambre = null;

if(isset($_GET['numero'])){
    $numero = $_GET['numero'];

    $bdd = getDatabase();

    $query = "SELECT * FROM chambres, tarifs WHERE numero=:n_umber";

    $statement = $bdd->prepare($query);

    $statement->bindParam(':n_umber', $numero);

    if($statement->execute()){
        $chambre = $statement->fetch(PDO::FETCH_OBJ);
    }
}

if ($chambre == null){
    echo 'chambre non trouvé';
} else {
    ?>
<form action="deleteChambre.php" method="post">
    <label for="capacite">Capacite :</label>
    <input type="text" name="capacite" disabled value="<?= $chambre->capacite?>"/> <br />
    <label for="exposition">Exposition :</label>
    <input type="text" name="exposition" disabled value="<?= $chambre->exposition?>"/> <br />
    <label for="douche">douche :</label>
    <input type="text" name="douche" disabled value="<?= $chambre->douche ?>"/> <br />
    <label for="etage">Etage :</label>
    <input type="text" name="etage"disabled value="<?= $chambre->etage?>"/> <br />
    <label for="tarif_id">Tarif :</label>
    <input type="text" name="tarif_id" disabled value="<?= $chambre->prix ?>"/> <br />

    <input type="hidden" name="numero" value="<?= $chambre->numero ?>" />
    <input type="submit" value="Valider">
</form>

<?php
}
?>
</body>
</html>
