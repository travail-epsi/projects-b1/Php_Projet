<?php
require('fonctions.php');

$pswd = password_hash($_POST['mdp'], PASSWORD_BCRYPT);
// Insertion dans la table
$query = "INSERT INTO admins( email, mdp, nom, prenom)
          VALUES(:p_mail, :p_mdp, :p_nom, :p_prenom)";

// Etape 1
$bdd = getDataBase();
// Etape 2.1 : prepare
$statement = $bdd->prepare($query);
// Etape 2.2 : paramètres
$statement->bindParam(':p_mail', $_POST['email']);
$statement->bindParam(':p_mdp', $pswd);
$statement->bindParam(':p_nom', $_POST['nom']);
$statement->bindParam(':p_prenom', $_POST['prenom']);

try {
    if ($statement->execute()) {
        header('Location: Connexion.php');
    }
} catch (Exception $exception) {
    echo '<script>alert("Impossible de créer un nouvel admin")</script>';
    echo '<script>window.location.replace("inscription.php");</script>';
}



