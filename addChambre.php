<?php

require ('fonctions.php');

$query= "INSERT INTO chambres(capacite, exposition, douche, etage, tarif_id)
        VALUES(:c_apacity, :e_xposition, :d_shower, :e_level, :t_id)";

$bdd = getDatabase();

$statement = $bdd->prepare($query);

$statement->bindParam(':c_apacity', $_POST['capacite']);
$statement->bindParam(':e_xposition', $_POST['exposition']);
$statement->bindParam(':d_shower', $_POST['douche']);
$statement->bindParam(':e_level', $_POST['etage']);
$statement->bindParam(':t_id', $_POST['tarif_id']);


if ($statement->execute()){
    header('Location: listeChambre.php');
} else {
    echo "Erreur lors de la redirection de la mise à jour";
}