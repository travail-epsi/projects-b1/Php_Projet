<?php
require 'fonctions.php';
$bdd = getDatabase();

$tarifs = null;
if ($bdd) {
    $tarifs = getMoney($bdd);
}
?>
<html>
<body>

<h1>Ajouter une chambre</h1>

<form action="addChambre.php" method="post" enctype="multipart/form-data">
    <label for="capacite">Capacite :</label>
    <input type="text" name="capacite" /> <br />
    <label for="exposition">Exposition :</label>
    <input type="text" name="exposition"/> <br />
    <label for="douche">douche :</label>
    <input type="text" name="douche"/> <br />
    <label for="etage">Etage :</label>
    <input type="text" name="etage"/> <br />
    <div class="form-group">
        <label>Tarifs:
            <select name="tarif_id" class="form-control">
                <option value=''>Tarifs</option>
                <?php
                if ($tarifs) {
                    foreach ($tarifs as $tarif) {
                        $option = '<option value="' . $tarif->id . '"';
                        $option .= '>' . $tarif->prix . '</option>';
                        echo $option;
                    }
                }
                ?>
            </select>
        </label>
    </div>





    <input type="submit" value="Valider">
</form>
</body>
</html>